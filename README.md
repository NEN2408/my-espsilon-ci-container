## Epsilon CI Container

This project provides the recipe for a [Docker container](https://en.wikipedia.org/wiki/Docker_(software)) that is able to run and builds in eclipse to carry out model-driven operations, primarily in the [Epsilon environment](https://www.eclipse.org/epsilon/). It is an implementation of the design provided in [this paper](https://link.springer.com/article/10.1007/s10270-022-01003-2) ([pre-print PDF here](https://arxiv.org/abs/2111.11607)) and explained [in this talk](https://www.youtube.com/watch?v=5p_VO1Ar0jc).

The implementation is currently in beta quality.

## Visuals

Below you can see the execution off the smoke test for the container. It consists of a number of workflows of the Epsilon suite. I'm constantly adding new orkloads here to see that I get the most in times of coverage.

![test](img/gitlab_tests.png)

## Components

The project is effectively just a composition of small bits. Below is a list:

### Container Definition

The container definition lives within the [`Dockerfile`](Dockerfile). It is designed to create an eclipse installer and then use the P2 installation system to create eventual container.

There is an [entry-point shell script](docker-entrypoint.sh) which is designed to mount an external directory via a dock volume called `workspace`. This group can also be used to copy an ant build file verbatim via standard in using the conventional `--` parameters as the first argument.

### Gitlab CI File

The [Gitlab CI file](.gitlab-ci.yml) is responsible for executing the Docker build functionality and pushing the resulting container to the [project's container registry](https://gitlab.com/jgsuess/epsilon-ci-container/container_registry/).

It uses Kaniko instead of docker tools for the build, because Kaniko does not require root demon permissions.

### Epsilon Tests

The [Epsilon suite tests](tests) are a set of examples copied out of the Epsilon suite. They are intended to serve as examples in how to use the system, monkey see, monkey do.

### Ooomph Configuration

The [Oomph configuration](oomph/configuration.setup) allows to install a complete IDE that is compatible with the container. This configuration is constantly being evolved. An Oomph configuration is a combination of an Eclipse Product like the Java Edition or Committers Version and a setup for a workspace.

## Usage

Some instructions for the components involved:

### IDE environment

To install the IDE environment:

1. [Download the Eclipse Installer](https://www.eclipse.org/downloads/packages/installer).
2. [Copy this link](https://gitlab.com/jgsuess/epsilon-ci-container/-/raw/main/oomph/configuration.setup?inline=false)
3. Start the installer.
4. Select "apply configuration" from the menu in the top right. This will **only** appear if the link is in the clipboard.

Some details on [Eclipse Installer Configurations](https://wiki.eclipse.org/Eclipse_Oomph_Authoring#Automation_and_Specialization_with_Configurations)

### Docker Container

The tag of the container is:

`registry.gitlab.com/jgsuess/epsilon-ci-container:latest`

[Instructions to run](https://docs.docker.com/engine/reference/commandline/run) will be added here later.

### Gitlab Execution

You can copy from the [Gitlab CI file](.gitlab-ci.yml). The `.execute_build` node is a template that is `extend`ed by the `workflow.flowchart`. Replace this later part with your own. Provide the path to the build file and the name of the target.

```yaml
.execute_build:
  stage: test
  image:
    name: registry.gitlab.com/jgsuess/epsilon-ci-container:latest
    entrypoint: [""]

    
workflow.flowchart:
  extends: .execute_build
  script:
    - /docker-entrypoint.sh tests/org.eclipse.epsilon.examples.workflow.flowchart/workflow.xml showPath
```

## Support
Please use [issues in this project](https://gitlab.com/jgsuess/epsilon-ci-container/-/issues) to contact me. This allows for nice threading.

## Roadmap
I want to grow this project and would love help.

* The most important thing for me is good documentation so we get more users and more bugs and issues. So if you can write or you know how to drive ChatGPT to produce nice, true and on-point output, you are extremely welcome. 
* The other big issue for me is to work out a proper versioning plan. I know this sounds academic, but it is the starting point for any ongoing quality.
* I would love to build a service version of this container, similar to a `REPL` loop that does not shut down after the run.

## Contributing

If you are a fan of MDE, CI/CD, DevOps, EMF, Epsilon or Eclipse, I would love you to work with me on this. 

## Authors and acknowledgment

We all stand on the shoulders of giants. The giants that spring to mind here are:

* The members of the Epsilon project team, especially Dimitris Kolovos
* The EMF maintainers, especially eternal heavy-lifter for the good of modelling Ed Merks.
* The makers of CDO and Oomph, dealing with complexity one architecture at a time, especially Eike Stepper.

Thanks for iceberg of which this project is the tip.

## License
EPL 2.0

## Project status
Alpha quality, actively maintained.